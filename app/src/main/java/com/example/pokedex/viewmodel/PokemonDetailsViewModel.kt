package com.example.pokedex.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokedex.view.DetailsFragment.DetailsState
import com.example.pokedexdata.model.repo.PokedexRepo
import kotlinx.coroutines.launch

class PokemonDetailsViewModel(private val repo: PokedexRepo): ViewModel() {

    private val _pokemonDetailsState: MutableLiveData<DetailsState> = MutableLiveData(DetailsState())
    val pokemonDetailsState: LiveData<DetailsState> get() = _pokemonDetailsState

    fun getPokemonDetails(name: String) {
        viewModelScope.launch {
            with(_pokemonDetailsState) {
                value = DetailsState(isLoading = true)
                value = DetailsState(repo.getPokemonDetails(name))
            }
        }

    }
}