package com.example.pokedex.viewmodel.vmfactory
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.pokedex.viewmodel.PokemonDetailsViewModel
import com.example.pokedex.viewmodel.PokemonListViewModel
import com.example.pokedexdata.model.repo.PokedexRepo

class DetailsVMFactory(
    private val repo: PokedexRepo
): ViewModelProvider.NewInstanceFactory() {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return PokemonDetailsViewModel(repo) as T
    }
}