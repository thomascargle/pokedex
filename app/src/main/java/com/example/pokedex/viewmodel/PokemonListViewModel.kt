package com.example.pokedex.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokedex.view.PokemonListFragment.PokemonListState
import com.example.pokedexdata.model.repo.PokedexRepo
import kotlinx.coroutines.launch

class PokemonListViewModel(private val repo: PokedexRepo): ViewModel() {
    private val _pokemonListState: MutableLiveData<PokemonListState> = MutableLiveData(PokemonListState())
    val pokemonListState: LiveData<PokemonListState> get() = _pokemonListState

    fun getPokemonList(typeNumber: Int) {
        viewModelScope.launch {
            with(_pokemonListState) {
                value = PokemonListState(isLoading = true)
                val pokemonInType = repo.getPokemonInType(typeNumber)
                if (pokemonInType.isNotEmpty()) {
                    value = value?.copy(specificTypeInfo = pokemonInType)
                } else {
                    value = PokemonListState(specificTypeInfo = pokemonInType)
                }
            }
        }
    }
}