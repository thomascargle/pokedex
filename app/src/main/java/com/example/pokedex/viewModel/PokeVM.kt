package com.example.pokedex.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.pokedex.fragments.PokeState
import kotlinx.coroutines.launch

class PokeVM:ViewModel() {

    private val pokestate  = MutableLiveData(PokeState(isLoading = true))

    val _pokestate : LiveData<PokeState> get() = pokestate

    }


