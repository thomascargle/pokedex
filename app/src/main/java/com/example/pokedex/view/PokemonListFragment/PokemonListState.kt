package com.example.pokedex.view.PokemonListFragment

import com.example.pokedexdata.model.objects.entities.SpecificTypeInfoEntity
import com.example.pokedexdata.model.objects.specifictypeinfo.Pokemon

data class PokemonListState(
    // Can change these to List<Pokemon> to fix the error in ViewModel
    val specificTypeInfo: List<Pokemon> = listOf(), // We will have the names of our pokemon in each type here
    val isLoading: Boolean = false,
)
