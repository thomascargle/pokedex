package com.example.pokedex.view.DetailsFragment

import com.example.pokedexdata.model.objects.entities.PokemonDetailsEntity
import com.example.pokedexdata.model.objects.pokemondetails.PokemonDetails

data class DetailsState(
    val pokemonDetails: PokemonDetailsEntity? = PokemonDetailsEntity(),
    val isLoading: Boolean? = false,
)