package com.example.pokedex.ui.theme
import android.app.Activity
import android.os.Build
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.foundation.isSystemInDarkTheme
class Theme {

    private val LightColorScheme = lightColorScheme(
        primary = lightBlue,
        secondary = lightGrey,
        tertiary = red
    )

    private val DarkColorScheme = darkColorScheme(
        primary = oxblood,
        secondary  = black,

    )
}