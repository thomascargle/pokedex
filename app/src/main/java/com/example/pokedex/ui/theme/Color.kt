package com.example.pokedex.ui.theme

import androidx.compose.ui.graphics.Color

val lightBlue = Color(0xFFBAC7FF)

val lightGrey = Color(0xFFAAAAAA)

val red = Color(0xFF0000)
val black = Color(0xFF000000)
val oxblood = Color(0x4A0404)

val HPColor = Color(0xFFF5FF00)
val AtkColor = Color( 1f, 0f,0f,0.66f)
val DefColor = Color(0f, 0f, 1f,0.44f)
val SpDefColor = Color(0.671f,0f,1f,0.57f)

val TypeNormal = Color(0xFFA8A77A)
val TypeFire = Color(0xFFEE8130)
val TypeWater = Color(0xFF639F0)
val TypeElectric = Color(0xFFF7AC74C)
val TypeIce = Color(0xFF96D9D6)
val TypeFighting = Color(0xFFC22E28)
val TypePoison = Color(0xFFA33EA1)




