package com.example.pokedex.fragments
import android.view.LayoutInflater
import androidx.recyclerview.widget.RecyclerView
import android.view.ViewGroup
import com.example.pokedex.databinding.ItemMovesBinding
class DetailAdapter:RecyclerView.Adapter<DetailAdapter.MoveViewHolder>() {

    inner class MoveViewHolder(val binding:ItemMovesBinding):
    RecyclerView.ViewHolder(binding.root){
        val nameOfMove = binding.tvMove

    }

    fun setData(){

    }

    override fun onCreateViewHolder(
        parent:ViewGroup,
        viewType:Int
    ):DetailAdapter.MoveViewHolder{
        return MoveViewHolder(ItemMovesBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: MoveViewHolder, position: Int) {
        TODO("Not yet implemented")
    }

    override fun getItemCount(): Int{
        TODO("Not yet implemented")
    }



}