package com.example.pokedex.fragments
import androidx.fragment.app.viewModels
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.lifecycle.lifecycleScope
import android.os.Bundle
import android.view.ViewGroup
import android.view.LayoutInflater
import kotlinx.coroutines.launch
import android.view.View
import com.example.pokedex.R
import dagger.hilt.android.AndroidEntryPoint
import com.example.pokedex.databinding.FragmentPoketypesBinding

import com.example.pokedex.viewModel.PokeVM
import javax.inject.Inject
@AndroidEntryPoint
class PoketypesFragment: Fragment(R.layout.fragment_poketypes) {

    lateinit var _binding:FragmentPoketypesBinding
    private val pokeTypeAdapter by lazy{
        TypeAdapter()
    }

    @Inject
    lateinit var viewModel: PokeVM

    private val binding get() = _binding!!

    private val pokevm by viewModels<PokeVM>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPoketypesBinding.inflate(inflater, container, false)
        return _binding.root
    }

    override  fun onViewCreated(view:View, savedInstanceState:Bundle?){
        super.onViewCreated(view, savedInstanceState)

    }

    private fun initObservers() = with(binding){
            lifecycleScope.launch{
                rvpoketypes.adapter = pokeTypeAdapter

            }
    }
}