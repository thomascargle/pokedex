package com.example.pokedex.fragments

data class PokeState (
    val isLoading: Boolean = false,
    var types:List<String> ?= null
)