package com.example.pokedex.fragments
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.example.pokedex.databinding.ItemPoketypeBinding
import androidx.recyclerview.widget.RecyclerView

class TypeAdapter(): RecyclerView.Adapter<TypeAdapter.PokeTypeViewHolder>() {

    class PokeTypeViewHolder(
        val binding: ItemPoketypeBinding,
        ):RecyclerView.ViewHolder(binding.root){
           fun getPokeType(){
               binding.tvPoketype.text
           }
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PokeTypeViewHolder {
        TODO("Not yet implemented")
    }

    override fun onBindViewHolder(holder: PokeTypeViewHolder, position: Int) {
        TODO("Not yet implemented")
    }

}