package com.example.pokedex.fragments
import androidx.fragment.app.viewModels
import android.util.Log
import android.view.View
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject
import com.example.pokedex.databinding.FragmentPokedetailsBinding

import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.example.pokedex.viewModel.PokeVM

@AndroidEntryPoint
class PokeDetailsFragment: Fragment() {

    lateinit var binding:FragmentPokedetailsBinding
@Inject
lateinit var viewModel:PokeVM
    private val moveadapter by lazy{
        DetailAdapter()
    }
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState:  Bundle?
    ):View?{
        binding = FragmentPokedetailsBinding.inflate(layoutInflater)
        binding.rvlistMoves.adapter = moveadapter
        binding.rvlistMoves.layoutManager = GridLayoutManager(this@PokeDetailsFragment.context,2)
        return binding.root
    }

    private fun initObservers(){

    }




}