package com.example.pokedex.fragments
import com.example.pokedex.databinding.FragmentStarterBinding
import androidx.fragment.app.Fragment
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.LayoutInflater
import com.example.pokedex.R
import androidx.navigation.fragment.findNavController
class StarterFragment:Fragment(R.layout.fragment_starter) {
    lateinit var _binding : FragmentStarterBinding

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater:LayoutInflater,
        container:ViewGroup?,
        savedInstanceState:Bundle?
    ) :View?{
        _binding=FragmentStarterBinding.inflate(inflater,container, false)
        return binding.root

    }

    override fun onViewCreated(view:View,savedInstanceState: Bundle?){
        binding.btnSearch.setOnClickListener {
            val action = StarterFragmentDirections.actionStarterFragmentToPoketypesFragment()
            findNavController().navigate(action)
        }

    }
}