package com.example.pokedexdata.model.local;

import android.database.Cursor;
import android.os.CancellationSignal;
import androidx.room.CoroutinesRoom;
import androidx.room.EntityInsertionAdapter;
import androidx.room.RoomDatabase;
import androidx.room.RoomSQLiteQuery;
import androidx.room.SharedSQLiteStatement;
import androidx.room.util.CursorUtil;
import androidx.room.util.DBUtil;
import androidx.sqlite.db.SupportSQLiteStatement;
import com.example.pokedexdata.model.objects.entities.PokemonDetailsEntity;
import com.example.pokedexdata.model.objects.entities.SpecificTypeInfoEntity;
import com.example.pokedexdata.model.objects.specifictypeinfo.Pokemon;
import java.lang.Class;
import java.lang.Exception;
import java.lang.Object;
import java.lang.Override;
import java.lang.String;
import java.lang.SuppressWarnings;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import kotlin.Unit;
import kotlin.coroutines.Continuation;

@SuppressWarnings({"unchecked", "deprecation"})
public final class PokedexDao_Impl implements PokedexDao {
  private final RoomDatabase __db;

  private final EntityInsertionAdapter<SpecificTypeInfoEntity> __insertionAdapterOfSpecificTypeInfoEntity;

  private final SharedSQLiteStatement __preparedStmtOfDeletePokemonInType;

  public PokedexDao_Impl(RoomDatabase __db) {
    this.__db = __db;
    this.__insertionAdapterOfSpecificTypeInfoEntity = new EntityInsertionAdapter<SpecificTypeInfoEntity>(__db) {
      @Override
      public String createQuery() {
        return "INSERT OR REPLACE INTO `SpecificTypeInfoEntity` (`name`,`pokemon`) VALUES (?,?)";
      }

      @Override
      public void bind(SupportSQLiteStatement stmt, SpecificTypeInfoEntity value) {
        if (value.getName() == null) {
          stmt.bindNull(1);
        } else {
          stmt.bindString(1, value.getName());
        }
        final String _tmp = Converters.INSTANCE.fromArrayListPokemon(value.getPokemon());
        if (_tmp == null) {
          stmt.bindNull(2);
        } else {
          stmt.bindString(2, _tmp);
        }
      }
    };
    this.__preparedStmtOfDeletePokemonInType = new SharedSQLiteStatement(__db) {
      @Override
      public String createQuery() {
        final String _query = "DELETE FROM SpecificTypeInfoEntity";
        return _query;
      }
    };
  }

  @Override
  public Object insertPokemonInType(final SpecificTypeInfoEntity[] specificTypeInfoEntity,
      final Continuation<? super Unit> continuation) {
    return CoroutinesRoom.execute(__db, true, new Callable<Unit>() {
      @Override
      public Unit call() throws Exception {
        __db.beginTransaction();
        try {
          __insertionAdapterOfSpecificTypeInfoEntity.insert(specificTypeInfoEntity);
          __db.setTransactionSuccessful();
          return Unit.INSTANCE;
        } finally {
          __db.endTransaction();
        }
      }
    }, continuation);
  }

  @Override
  public Object deletePokemonInType(final Continuation<? super Unit> continuation) {
    return CoroutinesRoom.execute(__db, true, new Callable<Unit>() {
      @Override
      public Unit call() throws Exception {
        final SupportSQLiteStatement _stmt = __preparedStmtOfDeletePokemonInType.acquire();
        __db.beginTransaction();
        try {
          _stmt.executeUpdateDelete();
          __db.setTransactionSuccessful();
          return Unit.INSTANCE;
        } finally {
          __db.endTransaction();
          __preparedStmtOfDeletePokemonInType.release(_stmt);
        }
      }
    }, continuation);
  }

  @Override
  public Object getPokemonInType(
      final Continuation<? super List<SpecificTypeInfoEntity>> continuation) {
    final String _sql = "SELECT * FROM SpecificTypeInfoEntity";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final CancellationSignal _cancellationSignal = DBUtil.createCancellationSignal();
    return CoroutinesRoom.execute(__db, false, _cancellationSignal, new Callable<List<SpecificTypeInfoEntity>>() {
      @Override
      public List<SpecificTypeInfoEntity> call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          final int _cursorIndexOfName = CursorUtil.getColumnIndexOrThrow(_cursor, "name");
          final int _cursorIndexOfPokemon = CursorUtil.getColumnIndexOrThrow(_cursor, "pokemon");
          final List<SpecificTypeInfoEntity> _result = new ArrayList<SpecificTypeInfoEntity>(_cursor.getCount());
          while(_cursor.moveToNext()) {
            final SpecificTypeInfoEntity _item;
            final String _tmpName;
            if (_cursor.isNull(_cursorIndexOfName)) {
              _tmpName = null;
            } else {
              _tmpName = _cursor.getString(_cursorIndexOfName);
            }
            final List<Pokemon> _tmpPokemon;
            final String _tmp;
            if (_cursor.isNull(_cursorIndexOfPokemon)) {
              _tmp = null;
            } else {
              _tmp = _cursor.getString(_cursorIndexOfPokemon);
            }
            _tmpPokemon = Converters.INSTANCE.fromStringPokemon(_tmp);
            _item = new SpecificTypeInfoEntity(_tmpName,_tmpPokemon);
            _result.add(_item);
          }
          return _result;
        } finally {
          _cursor.close();
          _statement.release();
        }
      }
    }, continuation);
  }

  @Override
  public Object getPokemonDetails(final Continuation<? super PokemonDetailsEntity> continuation) {
    final String _sql = "SELECT * FROM PokemonDetailsEntity";
    final RoomSQLiteQuery _statement = RoomSQLiteQuery.acquire(_sql, 0);
    final CancellationSignal _cancellationSignal = DBUtil.createCancellationSignal();
    return CoroutinesRoom.execute(__db, false, _cancellationSignal, new Callable<PokemonDetailsEntity>() {
      @Override
      public PokemonDetailsEntity call() throws Exception {
        final Cursor _cursor = DBUtil.query(__db, _statement, false, null);
        try {
          return _result;
        } finally {
          _cursor.close();
          _statement.release();
        }
      }
    }, continuation);
  }

  public static List<Class<?>> getRequiredConverters() {
    return Collections.emptyList();
  }
}
