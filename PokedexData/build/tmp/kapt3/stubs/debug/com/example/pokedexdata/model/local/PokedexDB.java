package com.example.pokedexdata.model.local;

import java.lang.System;

@androidx.room.TypeConverters(value = {com.example.pokedexdata.model.local.Converters.class})
@androidx.room.Database(entities = {com.example.pokedexdata.model.objects.entities.SpecificTypeInfoEntity.class}, version = 1)
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u0014\n\u0002\u0018\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\'\u0018\u0000 \u00052\u00020\u0001:\u0001\u0005B\u0005\u00a2\u0006\u0002\u0010\u0002J\b\u0010\u0003\u001a\u00020\u0004H&\u00a8\u0006\u0006"}, d2 = {"Lcom/example/pokedexdata/model/local/PokedexDB;", "Landroidx/room/RoomDatabase;", "()V", "pokedexDao", "Lcom/example/pokedexdata/model/local/PokedexDao;", "Companion", "PokedexData_debug"})
public abstract class PokedexDB extends androidx.room.RoomDatabase {
    @org.jetbrains.annotations.NotNull()
    public static final com.example.pokedexdata.model.local.PokedexDB.Companion Companion = null;
    private static final java.lang.String DATABASE_NAME = "pokedex.db";
    @kotlin.jvm.Volatile()
    private static volatile com.example.pokedexdata.model.local.PokedexDB instance;
    
    public PokedexDB() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public abstract com.example.pokedexdata.model.local.PokedexDao pokedexDao();
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0010\u0010\u0007\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\tH\u0002J\u000e\u0010\n\u001a\u00020\u00062\u0006\u0010\b\u001a\u00020\tR\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u0010\u0010\u0005\u001a\u0004\u0018\u00010\u0006X\u0082\u000e\u00a2\u0006\u0002\n\u0000\u00a8\u0006\u000b"}, d2 = {"Lcom/example/pokedexdata/model/local/PokedexDB$Companion;", "", "()V", "DATABASE_NAME", "", "instance", "Lcom/example/pokedexdata/model/local/PokedexDB;", "buildDatabase", "context", "Landroid/content/Context;", "getInstance", "PokedexData_debug"})
    public static final class Companion {
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.example.pokedexdata.model.local.PokedexDB getInstance(@org.jetbrains.annotations.NotNull()
        android.content.Context context) {
            return null;
        }
        
        private final com.example.pokedexdata.model.local.PokedexDB buildDatabase(android.content.Context context) {
            return null;
        }
    }
}