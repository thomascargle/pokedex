package com.example.pokedexdata.model.objects.specifictypeinfo;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000D\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0010\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0000\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001BY\u0012\f\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\f\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003\u0012\f\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0003\u0012\f\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0003\u0012\f\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00010\u0003\u0012\f\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u0003\u00a2\u0006\u0002\u0010\u000eJ\u000f\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u000f\u0010\u0017\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003H\u00c6\u0003J\u000f\u0010\u0018\u001a\b\u0012\u0004\u0012\u00020\b0\u0003H\u00c6\u0003J\u000f\u0010\u0019\u001a\b\u0012\u0004\u0012\u00020\n0\u0003H\u00c6\u0003J\u000f\u0010\u001a\u001a\b\u0012\u0004\u0012\u00020\u00010\u0003H\u00c6\u0003J\u000f\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\r0\u0003H\u00c6\u0003Ji\u0010\u001c\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\u000e\b\u0002\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u00032\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u00032\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\u000e\b\u0002\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00010\u00032\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u0003H\u00c6\u0001J\u0013\u0010\u001d\u001a\u00020\u001e2\b\u0010\u001f\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010 \u001a\u00020!H\u00d6\u0001J\t\u0010\"\u001a\u00020#H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000f\u0010\u0010R\u0017\u0010\u0005\u001a\b\u0012\u0004\u0012\u00020\u00060\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0011\u0010\u0010R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0012\u0010\u0010R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0013\u0010\u0010R\u0017\u0010\u000b\u001a\b\u0012\u0004\u0012\u00020\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0014\u0010\u0010R\u0017\u0010\f\u001a\b\u0012\u0004\u0012\u00020\r0\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0015\u0010\u0010\u00a8\u0006$"}, d2 = {"Lcom/example/pokedexdata/model/objects/specifictypeinfo/DamageRelations;", "", "double_damage_from", "", "Lcom/example/pokedexdata/model/objects/specifictypeinfo/DoubleDamageFrom;", "double_damage_to", "Lcom/example/pokedexdata/model/objects/specifictypeinfo/DoubleDamageTo;", "half_damage_from", "Lcom/example/pokedexdata/model/objects/specifictypeinfo/HalfDamageFrom;", "half_damage_to", "Lcom/example/pokedexdata/model/objects/specifictypeinfo/HalfDamageTo;", "no_damage_from", "no_damage_to", "Lcom/example/pokedexdata/model/objects/specifictypeinfo/NoDamageTo;", "(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V", "getDouble_damage_from", "()Ljava/util/List;", "getDouble_damage_to", "getHalf_damage_from", "getHalf_damage_to", "getNo_damage_from", "getNo_damage_to", "component1", "component2", "component3", "component4", "component5", "component6", "copy", "equals", "", "other", "hashCode", "", "toString", "", "PokedexData_debug"})
public final class DamageRelations {
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.DoubleDamageFrom> double_damage_from = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.DoubleDamageTo> double_damage_to = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.HalfDamageFrom> half_damage_from = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.HalfDamageTo> half_damage_to = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<java.lang.Object> no_damage_from = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.NoDamageTo> no_damage_to = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.pokedexdata.model.objects.specifictypeinfo.DamageRelations copy(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.DoubleDamageFrom> double_damage_from, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.DoubleDamageTo> double_damage_to, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.HalfDamageFrom> half_damage_from, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.HalfDamageTo> half_damage_to, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends java.lang.Object> no_damage_from, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.NoDamageTo> no_damage_to) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public DamageRelations(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.DoubleDamageFrom> double_damage_from, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.DoubleDamageTo> double_damage_to, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.HalfDamageFrom> half_damage_from, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.HalfDamageTo> half_damage_to, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends java.lang.Object> no_damage_from, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.NoDamageTo> no_damage_to) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.DoubleDamageFrom> component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.DoubleDamageFrom> getDouble_damage_from() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.DoubleDamageTo> component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.DoubleDamageTo> getDouble_damage_to() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.HalfDamageFrom> component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.HalfDamageFrom> getHalf_damage_from() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.HalfDamageTo> component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.HalfDamageTo> getHalf_damage_to() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.Object> component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.Object> getNo_damage_from() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.NoDamageTo> component6() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.NoDamageTo> getNo_damage_to() {
        return null;
    }
}