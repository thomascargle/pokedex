package com.example.pokedexdata.model.objects.pokemondetails;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\f\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0010\u000e\n\u0000\b\u0086\b\u0018\u00002\u00020\u0001B\u001d\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0005\u0012\u0006\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\u0002\u0010\bJ\t\u0010\u000f\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0005H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0007H\u00c6\u0003J\'\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00052\b\b\u0002\u0010\u0006\u001a\u00020\u0007H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0003H\u00d6\u0001J\t\u0010\u0017\u001a\u00020\u0018H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\t\u0010\nR\u0011\u0010\u0004\u001a\u00020\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u000b\u0010\fR\u0011\u0010\u0006\u001a\u00020\u0007\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000e\u00a8\u0006\u0019"}, d2 = {"Lcom/example/pokedexdata/model/objects/pokemondetails/VersionGroupDetail;", "", "level_learned_at", "", "move_learn_method", "Lcom/example/pokedexdata/model/objects/pokemondetails/MoveLearnMethod;", "version_group", "Lcom/example/pokedexdata/model/objects/pokemondetails/VersionGroup;", "(ILcom/example/pokedexdata/model/objects/pokemondetails/MoveLearnMethod;Lcom/example/pokedexdata/model/objects/pokemondetails/VersionGroup;)V", "getLevel_learned_at", "()I", "getMove_learn_method", "()Lcom/example/pokedexdata/model/objects/pokemondetails/MoveLearnMethod;", "getVersion_group", "()Lcom/example/pokedexdata/model/objects/pokemondetails/VersionGroup;", "component1", "component2", "component3", "copy", "equals", "", "other", "hashCode", "toString", "", "PokedexData_debug"})
public final class VersionGroupDetail {
    private final int level_learned_at = 0;
    @org.jetbrains.annotations.NotNull()
    private final com.example.pokedexdata.model.objects.pokemondetails.MoveLearnMethod move_learn_method = null;
    @org.jetbrains.annotations.NotNull()
    private final com.example.pokedexdata.model.objects.pokemondetails.VersionGroup version_group = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.pokedexdata.model.objects.pokemondetails.VersionGroupDetail copy(int level_learned_at, @org.jetbrains.annotations.NotNull()
    com.example.pokedexdata.model.objects.pokemondetails.MoveLearnMethod move_learn_method, @org.jetbrains.annotations.NotNull()
    com.example.pokedexdata.model.objects.pokemondetails.VersionGroup version_group) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public VersionGroupDetail(int level_learned_at, @org.jetbrains.annotations.NotNull()
    com.example.pokedexdata.model.objects.pokemondetails.MoveLearnMethod move_learn_method, @org.jetbrains.annotations.NotNull()
    com.example.pokedexdata.model.objects.pokemondetails.VersionGroup version_group) {
        super();
    }
    
    public final int component1() {
        return 0;
    }
    
    public final int getLevel_learned_at() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.pokedexdata.model.objects.pokemondetails.MoveLearnMethod component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.pokedexdata.model.objects.pokemondetails.MoveLearnMethod getMove_learn_method() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.pokedexdata.model.objects.pokemondetails.VersionGroup component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.pokedexdata.model.objects.pokemondetails.VersionGroup getVersion_group() {
        return null;
    }
}