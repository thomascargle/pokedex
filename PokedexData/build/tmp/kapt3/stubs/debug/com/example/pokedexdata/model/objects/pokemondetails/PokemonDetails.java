package com.example.pokedexdata.model.objects.pokemondetails;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000^\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0010\u000b\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0004\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0002\b3\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B\u00fd\u0001\u0012\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u0012\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u0012\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0003\u0012\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0003\u0012\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u0006\u0012\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00010\u0003\u0012\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u0006\u0012\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u0012\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u0012\u000e\b\u0002\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\u0003\u0012\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u0011\u0012\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u0006\u0012\u000e\b\u0002\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00010\u0003\u0012\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u0018\u0012\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u001a\u0012\u000e\b\u0002\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001c0\u0003\u0012\u000e\b\u0002\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0003\u0012\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\u0002\u0010 J\u000f\u0010:\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003H\u00c6\u0003J\u000f\u0010;\u001a\b\u0012\u0004\u0012\u00020\u00130\u0003H\u00c6\u0003J\u000b\u0010<\u001a\u0004\u0018\u00010\u0011H\u00c6\u0003J\u0010\u0010=\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003\u00a2\u0006\u0002\u0010$J\u000f\u0010>\u001a\b\u0012\u0004\u0012\u00020\u00010\u0003H\u00c6\u0003J\u000b\u0010?\u001a\u0004\u0018\u00010\u0018H\u00c6\u0003J\u000b\u0010@\u001a\u0004\u0018\u00010\u001aH\u00c6\u0003J\u000f\u0010A\u001a\b\u0012\u0004\u0012\u00020\u001c0\u0003H\u00c6\u0003J\u000f\u0010B\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0003H\u00c6\u0003J\u0010\u0010C\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003\u00a2\u0006\u0002\u0010$J\u0010\u0010D\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003\u00a2\u0006\u0002\u0010$J\u000f\u0010E\u001a\b\u0012\u0004\u0012\u00020\b0\u0003H\u00c6\u0003J\u000f\u0010F\u001a\b\u0012\u0004\u0012\u00020\n0\u0003H\u00c6\u0003J\u0010\u0010G\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003\u00a2\u0006\u0002\u0010$J\u000f\u0010H\u001a\b\u0012\u0004\u0012\u00020\u00010\u0003H\u00c6\u0003J\u0010\u0010I\u001a\u0004\u0018\u00010\u0006H\u00c6\u0003\u00a2\u0006\u0002\u0010$J\u0010\u0010J\u001a\u0004\u0018\u00010\u000fH\u00c6\u0003\u00a2\u0006\u0002\u0010+J\u000b\u0010K\u001a\u0004\u0018\u00010\u0011H\u00c6\u0003J\u0086\u0002\u0010L\u001a\u00020\u00002\u000e\b\u0002\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u00032\n\b\u0002\u0010\u0005\u001a\u0004\u0018\u00010\u00062\u000e\b\u0002\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u00032\u000e\b\u0002\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u00032\n\b\u0002\u0010\u000b\u001a\u0004\u0018\u00010\u00062\u000e\b\u0002\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00010\u00032\n\b\u0002\u0010\r\u001a\u0004\u0018\u00010\u00062\n\b\u0002\u0010\u000e\u001a\u0004\u0018\u00010\u000f2\n\b\u0002\u0010\u0010\u001a\u0004\u0018\u00010\u00112\u000e\b\u0002\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\u00032\n\b\u0002\u0010\u0014\u001a\u0004\u0018\u00010\u00112\n\b\u0002\u0010\u0015\u001a\u0004\u0018\u00010\u00062\u000e\b\u0002\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00010\u00032\n\b\u0002\u0010\u0017\u001a\u0004\u0018\u00010\u00182\n\b\u0002\u0010\u0019\u001a\u0004\u0018\u00010\u001a2\u000e\b\u0002\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001c0\u00032\u000e\b\u0002\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001e0\u00032\n\b\u0002\u0010\u001f\u001a\u0004\u0018\u00010\u0006H\u00c6\u0001\u00a2\u0006\u0002\u0010MJ\u0013\u0010N\u001a\u00020\u000f2\b\u0010O\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010P\u001a\u00020\u0006H\u00d6\u0001J\u0006\u0010Q\u001a\u00020RJ\t\u0010S\u001a\u00020\u0011H\u00d6\u0001R\u0017\u0010\u0002\u001a\b\u0012\u0004\u0012\u00020\u00040\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\"R\u0015\u0010\u0005\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\n\n\u0002\u0010%\u001a\u0004\b#\u0010$R\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\b0\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\"R\u0017\u0010\t\u001a\b\u0012\u0004\u0012\u00020\n0\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\'\u0010\"R\u0015\u0010\u000b\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\n\n\u0002\u0010%\u001a\u0004\b(\u0010$R\u0017\u0010\f\u001a\b\u0012\u0004\u0012\u00020\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b)\u0010\"R\u0015\u0010\r\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\n\n\u0002\u0010%\u001a\u0004\b*\u0010$R\u0015\u0010\u000e\u001a\u0004\u0018\u00010\u000f\u00a2\u0006\n\n\u0002\u0010,\u001a\u0004\b\u000e\u0010+R\u0013\u0010\u0010\u001a\u0004\u0018\u00010\u0011\u00a2\u0006\b\n\u0000\u001a\u0004\b-\u0010.R\u0017\u0010\u0012\u001a\b\u0012\u0004\u0012\u00020\u00130\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b/\u0010\"R\u0013\u0010\u0014\u001a\u0004\u0018\u00010\u0011\u00a2\u0006\b\n\u0000\u001a\u0004\b0\u0010.R\u0015\u0010\u0015\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\n\n\u0002\u0010%\u001a\u0004\b1\u0010$R\u0017\u0010\u0016\u001a\b\u0012\u0004\u0012\u00020\u00010\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b2\u0010\"R\u0013\u0010\u0017\u001a\u0004\u0018\u00010\u0018\u00a2\u0006\b\n\u0000\u001a\u0004\b3\u00104R\u0013\u0010\u0019\u001a\u0004\u0018\u00010\u001a\u00a2\u0006\b\n\u0000\u001a\u0004\b5\u00106R\u0017\u0010\u001b\u001a\b\u0012\u0004\u0012\u00020\u001c0\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b7\u0010\"R\u0017\u0010\u001d\u001a\b\u0012\u0004\u0012\u00020\u001e0\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b8\u0010\"R\u0015\u0010\u001f\u001a\u0004\u0018\u00010\u0006\u00a2\u0006\n\n\u0002\u0010%\u001a\u0004\b9\u0010$\u00a8\u0006T"}, d2 = {"Lcom/example/pokedexdata/model/objects/pokemondetails/PokemonDetails;", "", "abilities", "", "Lcom/example/pokedexdata/model/objects/pokemondetails/Ability;", "base_experience", "", "forms", "Lcom/example/pokedexdata/model/objects/pokemondetails/Form;", "game_indices", "Lcom/example/pokedexdata/model/objects/pokemondetails/GameIndice;", "height", "held_items", "id", "is_default", "", "location_area_encounters", "", "moves", "Lcom/example/pokedexdata/model/objects/pokemondetails/Move;", "name", "order", "past_types", "species", "Lcom/example/pokedexdata/model/objects/pokemondetails/Species;", "sprites", "Lcom/example/pokedexdata/model/objects/pokemondetails/Sprites;", "stats", "Lcom/example/pokedexdata/model/objects/pokemondetails/Stat;", "types", "Lcom/example/pokedexdata/model/objects/pokemondetails/Type;", "weight", "(Ljava/util/List;Ljava/lang/Integer;Ljava/util/List;Ljava/util/List;Ljava/lang/Integer;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/Integer;Ljava/util/List;Lcom/example/pokedexdata/model/objects/pokemondetails/Species;Lcom/example/pokedexdata/model/objects/pokemondetails/Sprites;Ljava/util/List;Ljava/util/List;Ljava/lang/Integer;)V", "getAbilities", "()Ljava/util/List;", "getBase_experience", "()Ljava/lang/Integer;", "Ljava/lang/Integer;", "getForms", "getGame_indices", "getHeight", "getHeld_items", "getId", "()Ljava/lang/Boolean;", "Ljava/lang/Boolean;", "getLocation_area_encounters", "()Ljava/lang/String;", "getMoves", "getName", "getOrder", "getPast_types", "getSpecies", "()Lcom/example/pokedexdata/model/objects/pokemondetails/Species;", "getSprites", "()Lcom/example/pokedexdata/model/objects/pokemondetails/Sprites;", "getStats", "getTypes", "getWeight", "component1", "component10", "component11", "component12", "component13", "component14", "component15", "component16", "component17", "component18", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "(Ljava/util/List;Ljava/lang/Integer;Ljava/util/List;Ljava/util/List;Ljava/lang/Integer;Ljava/util/List;Ljava/lang/Integer;Ljava/lang/Boolean;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/Integer;Ljava/util/List;Lcom/example/pokedexdata/model/objects/pokemondetails/Species;Lcom/example/pokedexdata/model/objects/pokemondetails/Sprites;Ljava/util/List;Ljava/util/List;Ljava/lang/Integer;)Lcom/example/pokedexdata/model/objects/pokemondetails/PokemonDetails;", "equals", "other", "hashCode", "toPokemonDetailsEntity", "Lcom/example/pokedexdata/model/objects/entities/PokemonDetailsEntity;", "toString", "PokedexData_debug"})
public final class PokemonDetails {
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Ability> abilities = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Integer base_experience = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Form> forms = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.example.pokedexdata.model.objects.pokemondetails.GameIndice> game_indices = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Integer height = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<java.lang.Object> held_items = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Integer id = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Boolean is_default = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String location_area_encounters = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Move> moves = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.String name = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Integer order = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<java.lang.Object> past_types = null;
    @org.jetbrains.annotations.Nullable()
    private final com.example.pokedexdata.model.objects.pokemondetails.Species species = null;
    @org.jetbrains.annotations.Nullable()
    private final com.example.pokedexdata.model.objects.pokemondetails.Sprites sprites = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Stat> stats = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Type> types = null;
    @org.jetbrains.annotations.Nullable()
    private final java.lang.Integer weight = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.pokedexdata.model.objects.pokemondetails.PokemonDetails copy(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Ability> abilities, @org.jetbrains.annotations.Nullable()
    java.lang.Integer base_experience, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Form> forms, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.pokemondetails.GameIndice> game_indices, @org.jetbrains.annotations.Nullable()
    java.lang.Integer height, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends java.lang.Object> held_items, @org.jetbrains.annotations.Nullable()
    java.lang.Integer id, @org.jetbrains.annotations.Nullable()
    java.lang.Boolean is_default, @org.jetbrains.annotations.Nullable()
    java.lang.String location_area_encounters, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Move> moves, @org.jetbrains.annotations.Nullable()
    java.lang.String name, @org.jetbrains.annotations.Nullable()
    java.lang.Integer order, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends java.lang.Object> past_types, @org.jetbrains.annotations.Nullable()
    com.example.pokedexdata.model.objects.pokemondetails.Species species, @org.jetbrains.annotations.Nullable()
    com.example.pokedexdata.model.objects.pokemondetails.Sprites sprites, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Stat> stats, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Type> types, @org.jetbrains.annotations.Nullable()
    java.lang.Integer weight) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public PokemonDetails() {
        super();
    }
    
    public PokemonDetails(@org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Ability> abilities, @org.jetbrains.annotations.Nullable()
    java.lang.Integer base_experience, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Form> forms, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.pokemondetails.GameIndice> game_indices, @org.jetbrains.annotations.Nullable()
    java.lang.Integer height, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends java.lang.Object> held_items, @org.jetbrains.annotations.Nullable()
    java.lang.Integer id, @org.jetbrains.annotations.Nullable()
    java.lang.Boolean is_default, @org.jetbrains.annotations.Nullable()
    java.lang.String location_area_encounters, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Move> moves, @org.jetbrains.annotations.Nullable()
    java.lang.String name, @org.jetbrains.annotations.Nullable()
    java.lang.Integer order, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends java.lang.Object> past_types, @org.jetbrains.annotations.Nullable()
    com.example.pokedexdata.model.objects.pokemondetails.Species species, @org.jetbrains.annotations.Nullable()
    com.example.pokedexdata.model.objects.pokemondetails.Sprites sprites, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Stat> stats, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Type> types, @org.jetbrains.annotations.Nullable()
    java.lang.Integer weight) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Ability> component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Ability> getAbilities() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component2() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getBase_experience() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Form> component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Form> getForms() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.pokemondetails.GameIndice> component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.pokemondetails.GameIndice> getGame_indices() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component5() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getHeight() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.Object> component6() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.Object> getHeld_items() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component7() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getId() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean component8() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Boolean is_default() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component9() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getLocation_area_encounters() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Move> component10() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Move> getMoves() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String component11() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.String getName() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component12() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getOrder() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.Object> component13() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.Object> getPast_types() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.pokedexdata.model.objects.pokemondetails.Species component14() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.pokedexdata.model.objects.pokemondetails.Species getSpecies() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.pokedexdata.model.objects.pokemondetails.Sprites component15() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final com.example.pokedexdata.model.objects.pokemondetails.Sprites getSprites() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Stat> component16() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Stat> getStats() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Type> component17() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.pokemondetails.Type> getTypes() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer component18() {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Integer getWeight() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.pokedexdata.model.objects.entities.PokemonDetailsEntity toPokemonDetailsEntity() {
        return null;
    }
}