package com.example.pokedexdata.model.remote;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000.\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0003\bf\u0018\u0000 \u000f2\u00020\u0001:\u0001\u000fJ\u001b\u0010\u0002\u001a\u00020\u00032\b\b\u0001\u0010\u0004\u001a\u00020\u0005H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0006J\u001b\u0010\u0007\u001a\u00020\b2\b\b\u0001\u0010\t\u001a\u00020\nH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000bJ\u0011\u0010\f\u001a\u00020\rH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u000e\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0010"}, d2 = {"Lcom/example/pokedexdata/model/remote/PokedexService;", "", "getPokemonDetails", "Lcom/example/pokedexdata/model/objects/pokemondetails/PokemonDetails;", "name", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getPokemonInType", "Lcom/example/pokedexdata/model/objects/specifictypeinfo/SpecificTypeInfo;", "typeNumber", "", "(ILkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getTypes", "Lcom/example/pokedexdata/model/objects/alltypelist/AllTypeList;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "Companion", "PokedexData_debug"})
public abstract interface PokedexService {
    @org.jetbrains.annotations.NotNull()
    public static final com.example.pokedexdata.model.remote.PokedexService.Companion Companion = null;
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "type")
    public abstract java.lang.Object getTypes(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.example.pokedexdata.model.objects.alltypelist.AllTypeList> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "type/{typeNumber}")
    public abstract java.lang.Object getPokemonInType(@retrofit2.http.Path(value = "typeNumber")
    int typeNumber, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.example.pokedexdata.model.objects.specifictypeinfo.SpecificTypeInfo> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @retrofit2.http.GET(value = "pokemon/{name}")
    public abstract java.lang.Object getPokemonDetails(@org.jetbrains.annotations.NotNull()
    @retrofit2.http.Path(value = "name")
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.example.pokedexdata.model.objects.pokemondetails.PokemonDetails> continuation);
    
    @kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001a\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\b\u0086\u0003\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u0006\u0010\u0006\u001a\u00020\u0007R\u000e\u0010\u0003\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0005\u001a\u00020\u0004X\u0082T\u00a2\u0006\u0002\n\u0000\u00a8\u0006\b"}, d2 = {"Lcom/example/pokedexdata/model/remote/PokedexService$Companion;", "", "()V", "BASE_URL", "", "TYPE_ENDPOINT", "getInstance", "Lcom/example/pokedexdata/model/remote/PokedexService;", "PokedexData_debug"})
    public static final class Companion {
        private static final java.lang.String BASE_URL = "https://pokeapi.co/api/v2/";
        private static final java.lang.String TYPE_ENDPOINT = "type";
        
        private Companion() {
            super();
        }
        
        @org.jetbrains.annotations.NotNull()
        public final com.example.pokedexdata.model.remote.PokedexService getInstance() {
            return null;
        }
    }
}