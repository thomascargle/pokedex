package com.example.pokedexdata.model.local;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\u001e\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\u000e\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0003\b\u00c6\u0002\u0018\u00002\u00020\u0001B\u0007\b\u0002\u00a2\u0006\u0002\u0010\u0002J\u001a\u0010\u0003\u001a\u00020\u00042\u0010\u0010\u0005\u001a\f\u0012\u0006\u0012\u0004\u0018\u00010\u0007\u0018\u00010\u0006H\u0007J\u0018\u0010\b\u001a\b\u0012\u0004\u0012\u00020\u00070\u00062\b\u0010\t\u001a\u0004\u0018\u00010\u0004H\u0007\u00a8\u0006\n"}, d2 = {"Lcom/example/pokedexdata/model/local/Converters;", "", "()V", "fromArrayListPokemon", "", "list", "", "Lcom/example/pokedexdata/model/objects/specifictypeinfo/Pokemon;", "fromStringPokemon", "value", "PokedexData_debug"})
public final class Converters {
    @org.jetbrains.annotations.NotNull()
    public static final com.example.pokedexdata.model.local.Converters INSTANCE = null;
    
    private Converters() {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.TypeConverter()
    public final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.Pokemon> fromStringPokemon(@org.jetbrains.annotations.Nullable()
    java.lang.String value) {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    @androidx.room.TypeConverter()
    public final java.lang.String fromArrayListPokemon(@org.jetbrains.annotations.Nullable()
    java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.Pokemon> list) {
        return null;
    }
}