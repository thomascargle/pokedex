package com.example.pokedexdata.model.repo;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000F\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0002\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u0002\u0018\u00002\u00020\u0001B\r\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\u0002\u0010\u0004J\u0019\u0010\t\u001a\u00020\n2\u0006\u0010\u000b\u001a\u00020\fH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\rJ\u001f\u0010\u000e\u001a\b\u0012\u0004\u0012\u00020\u00100\u000f2\u0006\u0010\u0011\u001a\u00020\u0012H\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0013J\u0017\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\u000fH\u0086@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0016R\u000e\u0010\u0005\u001a\u00020\u0006X\u0082\u0004\u00a2\u0006\u0002\n\u0000R\u000e\u0010\u0007\u001a\u00020\bX\u0082\u0004\u00a2\u0006\u0002\n\u0000\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u0017"}, d2 = {"Lcom/example/pokedexdata/model/repo/PokedexRepo;", "", "context", "Landroid/content/Context;", "(Landroid/content/Context;)V", "pokedexDao", "Lcom/example/pokedexdata/model/local/PokedexDao;", "pokedexService", "Lcom/example/pokedexdata/model/remote/PokedexService;", "getPokemonDetails", "Lcom/example/pokedexdata/model/objects/entities/PokemonDetailsEntity;", "name", "", "(Ljava/lang/String;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getPokemonInType", "", "Lcom/example/pokedexdata/model/objects/specifictypeinfo/Pokemon;", "typeNumber", "", "(ILkotlin/coroutines/Continuation;)Ljava/lang/Object;", "restorePokemonInType", "Lcom/example/pokedexdata/model/objects/entities/SpecificTypeInfoEntity;", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "PokedexData_debug"})
public final class PokedexRepo {
    private final com.example.pokedexdata.model.remote.PokedexService pokedexService = null;
    private final com.example.pokedexdata.model.local.PokedexDao pokedexDao = null;
    
    public PokedexRepo(@org.jetbrains.annotations.NotNull()
    android.content.Context context) {
        super();
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getPokemonInType(int typeNumber, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.Pokemon>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object restorePokemonInType(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.List<com.example.pokedexdata.model.objects.entities.SpecificTypeInfoEntity>> continuation) {
        return null;
    }
    
    @org.jetbrains.annotations.Nullable()
    public final java.lang.Object getPokemonDetails(@org.jetbrains.annotations.NotNull()
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.example.pokedexdata.model.objects.entities.PokemonDetailsEntity> continuation) {
        return null;
    }
}