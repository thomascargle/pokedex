package com.example.pokedexdata.model.objects.pokemondetails;

import java.lang.System;

@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000\"\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u000e\n\u0002\b\u0010\n\u0002\u0010\u000b\n\u0002\b\u0002\n\u0002\u0010\b\n\u0002\b\u0002\b\u0086\b\u0018\u00002\u00020\u0001B%\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\u0006\u0010\u0004\u001a\u00020\u0001\u0012\u0006\u0010\u0005\u001a\u00020\u0003\u0012\u0006\u0010\u0006\u001a\u00020\u0001\u00a2\u0006\u0002\u0010\u0007J\t\u0010\u000e\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u000f\u001a\u00020\u0001H\u00c6\u0003J\t\u0010\u0010\u001a\u00020\u0003H\u00c6\u0003J\t\u0010\u0011\u001a\u00020\u0001H\u00c6\u0003J1\u0010\u0012\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\b\b\u0002\u0010\u0004\u001a\u00020\u00012\b\b\u0002\u0010\u0005\u001a\u00020\u00032\b\b\u0002\u0010\u0006\u001a\u00020\u0001H\u00c6\u0001J\u0013\u0010\u0013\u001a\u00020\u00142\b\u0010\u0015\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u0010\u0016\u001a\u00020\u0017H\u00d6\u0001J\t\u0010\u0018\u001a\u00020\u0003H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\b\u0010\tR\u0011\u0010\u0004\u001a\u00020\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b\n\u0010\u000bR\u0011\u0010\u0005\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\f\u0010\tR\u0011\u0010\u0006\u001a\u00020\u0001\u00a2\u0006\b\n\u0000\u001a\u0004\b\r\u0010\u000b\u00a8\u0006\u0019"}, d2 = {"Lcom/example/pokedexdata/model/objects/pokemondetails/UltraSunUltraMoon;", "", "front_default", "", "front_female", "front_shiny", "front_shiny_female", "(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)V", "getFront_default", "()Ljava/lang/String;", "getFront_female", "()Ljava/lang/Object;", "getFront_shiny", "getFront_shiny_female", "component1", "component2", "component3", "component4", "copy", "equals", "", "other", "hashCode", "", "toString", "PokedexData_debug"})
public final class UltraSunUltraMoon {
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String front_default = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.Object front_female = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String front_shiny = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.Object front_shiny_female = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.pokedexdata.model.objects.pokemondetails.UltraSunUltraMoon copy(@org.jetbrains.annotations.NotNull()
    java.lang.String front_default, @org.jetbrains.annotations.NotNull()
    java.lang.Object front_female, @org.jetbrains.annotations.NotNull()
    java.lang.String front_shiny, @org.jetbrains.annotations.NotNull()
    java.lang.Object front_shiny_female) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public UltraSunUltraMoon(@org.jetbrains.annotations.NotNull()
    java.lang.String front_default, @org.jetbrains.annotations.NotNull()
    java.lang.Object front_female, @org.jetbrains.annotations.NotNull()
    java.lang.String front_shiny, @org.jetbrains.annotations.NotNull()
    java.lang.Object front_shiny_female) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getFront_default() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object getFront_female() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getFront_shiny() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object component4() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.Object getFront_shiny_female() {
        return null;
    }
}