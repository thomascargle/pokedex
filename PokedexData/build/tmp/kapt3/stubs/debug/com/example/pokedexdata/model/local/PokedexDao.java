package com.example.pokedexdata.model.local;

import java.lang.System;

@androidx.room.Dao()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000,\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0010\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0010\u0011\n\u0002\b\u0002\bg\u0018\u00002\u00020\u0001J\u0011\u0010\u0002\u001a\u00020\u0003H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0004J\u0011\u0010\u0005\u001a\u00020\u0006H\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0004J\u0017\u0010\u0007\u001a\b\u0012\u0004\u0012\u00020\t0\bH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\u0004J%\u0010\n\u001a\u00020\u00032\u0012\u0010\u000b\u001a\n\u0012\u0006\b\u0001\u0012\u00020\t0\f\"\u00020\tH\u00a7@\u00f8\u0001\u0000\u00a2\u0006\u0002\u0010\r\u0082\u0002\u0004\n\u0002\b\u0019\u00a8\u0006\u000e"}, d2 = {"Lcom/example/pokedexdata/model/local/PokedexDao;", "", "deletePokemonInType", "", "(Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "getPokemonDetails", "Lcom/example/pokedexdata/model/objects/entities/PokemonDetailsEntity;", "getPokemonInType", "", "Lcom/example/pokedexdata/model/objects/entities/SpecificTypeInfoEntity;", "insertPokemonInType", "specificTypeInfoEntity", "", "([Lcom/example/pokedexdata/model/objects/entities/SpecificTypeInfoEntity;Lkotlin/coroutines/Continuation;)Ljava/lang/Object;", "PokedexData_debug"})
public abstract interface PokedexDao {
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.Insert(onConflict = androidx.room.OnConflictStrategy.REPLACE)
    public abstract java.lang.Object insertPokemonInType(@org.jetbrains.annotations.NotNull()
    com.example.pokedexdata.model.objects.entities.SpecificTypeInfoEntity[] specificTypeInfoEntity, @org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.Query(value = "SELECT * FROM SpecificTypeInfoEntity")
    public abstract java.lang.Object getPokemonInType(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super java.util.List<com.example.pokedexdata.model.objects.entities.SpecificTypeInfoEntity>> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.Query(value = "SELECT * FROM PokemonDetailsEntity")
    public abstract java.lang.Object getPokemonDetails(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super com.example.pokedexdata.model.objects.entities.PokemonDetailsEntity> continuation);
    
    @org.jetbrains.annotations.Nullable()
    @androidx.room.Query(value = "DELETE FROM SpecificTypeInfoEntity")
    public abstract java.lang.Object deletePokemonInType(@org.jetbrains.annotations.NotNull()
    kotlin.coroutines.Continuation<? super kotlin.Unit> continuation);
}