package com.example.pokedexdata.model.objects.specifictypeinfo;

import java.lang.System;

@androidx.room.Entity()
@kotlin.Metadata(mv = {1, 6, 0}, k = 1, d1 = {"\u0000X\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010 \n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\b\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0018\u0002\n\u0000\n\u0002\u0010\u000e\n\u0000\n\u0002\u0018\u0002\n\u0002\b\u0002\n\u0002\u0018\u0002\n\u0002\b\u001d\n\u0002\u0010\u000b\n\u0002\b\u0003\n\u0002\u0018\u0002\n\u0002\b\u0002\b\u0087\b\u0018\u00002\u00020\u0001Bs\u0012\u0006\u0010\u0002\u001a\u00020\u0003\u0012\f\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u0012\u0006\u0010\u0007\u001a\u00020\b\u0012\u0006\u0010\t\u001a\u00020\n\u0012\u0006\u0010\u000b\u001a\u00020\f\u0012\f\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0005\u0012\u0006\u0010\u000f\u001a\u00020\u0010\u0012\f\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\u0005\u0012\f\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00010\u0005\u0012\f\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\u0005\u00a2\u0006\u0002\u0010\u0016J\t\u0010\'\u001a\u00020\u0003H\u00c6\u0003J\u000f\u0010(\u001a\b\u0012\u0004\u0012\u00020\u00150\u0005H\u00c6\u0003J\u000f\u0010)\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005H\u00c6\u0003J\t\u0010*\u001a\u00020\bH\u00c6\u0003J\t\u0010+\u001a\u00020\nH\u00c6\u0003J\t\u0010,\u001a\u00020\fH\u00c6\u0003J\u000f\u0010-\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0005H\u00c6\u0003J\t\u0010.\u001a\u00020\u0010H\u00c6\u0003J\u000f\u0010/\u001a\b\u0012\u0004\u0012\u00020\u00120\u0005H\u00c6\u0003J\u000f\u00100\u001a\b\u0012\u0004\u0012\u00020\u00010\u0005H\u00c6\u0003J\u008b\u0001\u00101\u001a\u00020\u00002\b\b\u0002\u0010\u0002\u001a\u00020\u00032\u000e\b\u0002\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u00052\b\b\u0002\u0010\u0007\u001a\u00020\b2\b\b\u0002\u0010\t\u001a\u00020\n2\b\b\u0002\u0010\u000b\u001a\u00020\f2\u000e\b\u0002\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\u00052\b\b\u0002\u0010\u000f\u001a\u00020\u00102\u000e\b\u0002\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\u00052\u000e\b\u0002\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00010\u00052\u000e\b\u0002\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\u0005H\u00c6\u0001J\u0013\u00102\u001a\u0002032\b\u00104\u001a\u0004\u0018\u00010\u0001H\u00d6\u0003J\t\u00105\u001a\u00020\nH\u00d6\u0001J\u0006\u00106\u001a\u000207J\t\u00108\u001a\u00020\u0010H\u00d6\u0001R\u0011\u0010\u0002\u001a\u00020\u0003\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0017\u0010\u0018R\u0017\u0010\u0004\u001a\b\u0012\u0004\u0012\u00020\u00060\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b\u0019\u0010\u001aR\u0011\u0010\u0007\u001a\u00020\b\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001b\u0010\u001cR\u0011\u0010\t\u001a\u00020\n\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001d\u0010\u001eR\u0011\u0010\u000b\u001a\u00020\f\u00a2\u0006\b\n\u0000\u001a\u0004\b\u001f\u0010 R\u0017\u0010\r\u001a\b\u0012\u0004\u0012\u00020\u000e0\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b!\u0010\u001aR\u0011\u0010\u000f\u001a\u00020\u0010\u00a2\u0006\b\n\u0000\u001a\u0004\b\"\u0010#R\u0017\u0010\u0011\u001a\b\u0012\u0004\u0012\u00020\u00120\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b$\u0010\u001aR\u0017\u0010\u0013\u001a\b\u0012\u0004\u0012\u00020\u00010\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b%\u0010\u001aR\u0017\u0010\u0014\u001a\b\u0012\u0004\u0012\u00020\u00150\u0005\u00a2\u0006\b\n\u0000\u001a\u0004\b&\u0010\u001a\u00a8\u00069"}, d2 = {"Lcom/example/pokedexdata/model/objects/specifictypeinfo/SpecificTypeInfo;", "", "damage_relations", "Lcom/example/pokedexdata/model/objects/specifictypeinfo/DamageRelations;", "game_indices", "", "Lcom/example/pokedexdata/model/objects/specifictypeinfo/GameIndice;", "generation", "Lcom/example/pokedexdata/model/objects/specifictypeinfo/GenerationX;", "id", "", "move_damage_class", "Lcom/example/pokedexdata/model/objects/specifictypeinfo/MoveDamageClass;", "moves", "Lcom/example/pokedexdata/model/objects/specifictypeinfo/Move;", "name", "", "names", "Lcom/example/pokedexdata/model/objects/specifictypeinfo/Name;", "past_damage_relations", "pokemon", "Lcom/example/pokedexdata/model/objects/specifictypeinfo/Pokemon;", "(Lcom/example/pokedexdata/model/objects/specifictypeinfo/DamageRelations;Ljava/util/List;Lcom/example/pokedexdata/model/objects/specifictypeinfo/GenerationX;ILcom/example/pokedexdata/model/objects/specifictypeinfo/MoveDamageClass;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;)V", "getDamage_relations", "()Lcom/example/pokedexdata/model/objects/specifictypeinfo/DamageRelations;", "getGame_indices", "()Ljava/util/List;", "getGeneration", "()Lcom/example/pokedexdata/model/objects/specifictypeinfo/GenerationX;", "getId", "()I", "getMove_damage_class", "()Lcom/example/pokedexdata/model/objects/specifictypeinfo/MoveDamageClass;", "getMoves", "getName", "()Ljava/lang/String;", "getNames", "getPast_damage_relations", "getPokemon", "component1", "component10", "component2", "component3", "component4", "component5", "component6", "component7", "component8", "component9", "copy", "equals", "", "other", "hashCode", "toSpecificTypeInfoEntity", "Lcom/example/pokedexdata/model/objects/entities/SpecificTypeInfoEntity;", "toString", "PokedexData_debug"})
public final class SpecificTypeInfo {
    @org.jetbrains.annotations.NotNull()
    private final com.example.pokedexdata.model.objects.specifictypeinfo.DamageRelations damage_relations = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.GameIndice> game_indices = null;
    @org.jetbrains.annotations.NotNull()
    private final com.example.pokedexdata.model.objects.specifictypeinfo.GenerationX generation = null;
    private final int id = 0;
    @org.jetbrains.annotations.NotNull()
    private final com.example.pokedexdata.model.objects.specifictypeinfo.MoveDamageClass move_damage_class = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.Move> moves = null;
    @org.jetbrains.annotations.NotNull()
    private final java.lang.String name = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.Name> names = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<java.lang.Object> past_damage_relations = null;
    @org.jetbrains.annotations.NotNull()
    private final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.Pokemon> pokemon = null;
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.pokedexdata.model.objects.specifictypeinfo.SpecificTypeInfo copy(@org.jetbrains.annotations.NotNull()
    com.example.pokedexdata.model.objects.specifictypeinfo.DamageRelations damage_relations, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.GameIndice> game_indices, @org.jetbrains.annotations.NotNull()
    com.example.pokedexdata.model.objects.specifictypeinfo.GenerationX generation, int id, @org.jetbrains.annotations.NotNull()
    com.example.pokedexdata.model.objects.specifictypeinfo.MoveDamageClass move_damage_class, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.Move> moves, @org.jetbrains.annotations.NotNull()
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.Name> names, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends java.lang.Object> past_damage_relations, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.Pokemon> pokemon) {
        return null;
    }
    
    @java.lang.Override()
    public boolean equals(@org.jetbrains.annotations.Nullable()
    java.lang.Object other) {
        return false;
    }
    
    @java.lang.Override()
    public int hashCode() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    @java.lang.Override()
    public java.lang.String toString() {
        return null;
    }
    
    public SpecificTypeInfo(@org.jetbrains.annotations.NotNull()
    com.example.pokedexdata.model.objects.specifictypeinfo.DamageRelations damage_relations, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.GameIndice> game_indices, @org.jetbrains.annotations.NotNull()
    com.example.pokedexdata.model.objects.specifictypeinfo.GenerationX generation, int id, @org.jetbrains.annotations.NotNull()
    com.example.pokedexdata.model.objects.specifictypeinfo.MoveDamageClass move_damage_class, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.Move> moves, @org.jetbrains.annotations.NotNull()
    java.lang.String name, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.Name> names, @org.jetbrains.annotations.NotNull()
    java.util.List<? extends java.lang.Object> past_damage_relations, @org.jetbrains.annotations.NotNull()
    java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.Pokemon> pokemon) {
        super();
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.pokedexdata.model.objects.specifictypeinfo.DamageRelations component1() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.pokedexdata.model.objects.specifictypeinfo.DamageRelations getDamage_relations() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.GameIndice> component2() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.GameIndice> getGame_indices() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.pokedexdata.model.objects.specifictypeinfo.GenerationX component3() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.pokedexdata.model.objects.specifictypeinfo.GenerationX getGeneration() {
        return null;
    }
    
    public final int component4() {
        return 0;
    }
    
    public final int getId() {
        return 0;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.pokedexdata.model.objects.specifictypeinfo.MoveDamageClass component5() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.pokedexdata.model.objects.specifictypeinfo.MoveDamageClass getMove_damage_class() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.Move> component6() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.Move> getMoves() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String component7() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.lang.String getName() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.Name> component8() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.Name> getNames() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.Object> component9() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<java.lang.Object> getPast_damage_relations() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.Pokemon> component10() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final java.util.List<com.example.pokedexdata.model.objects.specifictypeinfo.Pokemon> getPokemon() {
        return null;
    }
    
    @org.jetbrains.annotations.NotNull()
    public final com.example.pokedexdata.model.objects.entities.SpecificTypeInfoEntity toSpecificTypeInfoEntity() {
        return null;
    }
}