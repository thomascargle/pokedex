package com.example.pokedexdata.model.objects.pokemondetails

data class Move(
    val move: MoveX,
    val version_group_details: List<VersionGroupDetail>
)