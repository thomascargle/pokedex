package com.example.pokedexdata.model.objects.specifictypeinfo

data class DoubleDamageFrom(
    val name: String,
    val url: String
)