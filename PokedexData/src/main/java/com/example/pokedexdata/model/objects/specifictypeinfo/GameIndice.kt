package com.example.pokedexdata.model.objects.specifictypeinfo

data class GameIndice(
    val game_index: Int,
    val generation: Generation
)