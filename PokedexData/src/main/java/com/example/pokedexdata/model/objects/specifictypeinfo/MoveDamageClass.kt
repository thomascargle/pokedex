package com.example.pokedexdata.model.objects.specifictypeinfo

data class MoveDamageClass(
    val name: String,
    val url: String
)