package com.example.pokedexdata.model.objects.pokemondetails

data class Species(
    val name: String,
    val url: String
)