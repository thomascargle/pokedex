package com.example.pokedexdata.model.objects.specifictypeinfo

data class Name(
    val language: Language,
    val name: String
)