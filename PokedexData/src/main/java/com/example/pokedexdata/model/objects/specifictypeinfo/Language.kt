package com.example.pokedexdata.model.objects.specifictypeinfo

data class Language(
    val name: String,
    val url: String
)