package com.example.pokedexdata.model.objects.pokemondetails

data class Version(
    val name: String,
    val url: String
)