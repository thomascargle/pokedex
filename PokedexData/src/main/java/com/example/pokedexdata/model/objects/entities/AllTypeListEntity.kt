package com.example.pokedexdata.model.objects.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.pokedexdata.model.objects.alltypelist.Type

@Entity
data class AllTypeListEntity(
    @PrimaryKey
    val id: Int,
    val types: List<Type>
)
