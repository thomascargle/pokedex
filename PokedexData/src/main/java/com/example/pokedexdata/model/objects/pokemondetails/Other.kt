package com.example.pokedexdata.model.objects.pokemondetails

data class Other(
    val dream_World: String = "",
    val home: Home,
    val officialArtwork: String = ""
)