package com.example.pokedexdata.model.objects.pokemondetails

data class VersionGroup(
    val name: String,
    val url: String
)