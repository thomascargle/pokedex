package com.example.pokedexdata.model.objects.pokemondetails

data class MoveX(
    val name: String,
    val url: String
)