package com.example.pokedexdata.model.objects.pokemondetails

data class AbilityX(
    val name: String,
    val url: String
)