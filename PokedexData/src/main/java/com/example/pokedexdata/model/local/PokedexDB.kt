package com.example.pokedexdata.model.local

import android.content.Context
import androidx.room.*
import com.example.pokedexdata.model.objects.entities.SpecificTypeInfoEntity
import com.example.pokedexdata.model.objects.specifictypeinfo.Pokemon
import com.google.gson.Gson

import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


@Database(entities = [SpecificTypeInfoEntity::class], version = 1)
@TypeConverters(Converters::class)
abstract class PokedexDB : RoomDatabase() {
    abstract fun pokedexDao(): PokedexDao

    companion object {
        private const val DATABASE_NAME = "pokedex.db"

        @Volatile
        private var instance: PokedexDB? = null

        fun getInstance(context: Context): PokedexDB {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also {
                    instance = it
                }
            }
        }

        private fun buildDatabase(context: Context): PokedexDB {
            return Room
                .databaseBuilder(context, PokedexDB::class.java, DATABASE_NAME)
                .build()
        }
    }
}

object Converters {

    @TypeConverter
    fun fromStringPokemon(value: String?): List<Pokemon> {
        val listType: Type = object : TypeToken<List<Pokemon?>?>() {}.type
        return Gson().fromJson(value, listType)
    }

    @TypeConverter
    fun fromArrayListPokemon(list: List<Pokemon?>?): String {
        val gson = Gson()
        return gson.toJson(list)
    }
}
