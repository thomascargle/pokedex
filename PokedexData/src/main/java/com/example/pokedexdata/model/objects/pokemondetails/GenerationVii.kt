package com.example.pokedexdata.model.objects.pokemondetails

data class GenerationVii(
    val icons: String = "",
    val ultraSunUltraMoon: String = ""
)