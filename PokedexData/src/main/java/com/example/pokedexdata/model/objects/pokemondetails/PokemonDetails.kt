package com.example.pokedexdata.model.objects.pokemondetails

import com.example.pokedexdata.model.objects.entities.PokemonDetailsEntity

data class PokemonDetails(
    val abilities: List<Ability> = emptyList(),
    val base_experience: Int? = null,
    val forms: List<Form> = listOf(),
    val game_indices: List<GameIndice> = emptyList(),
    val height: Int? = null,
    val held_items: List<Any> = emptyList(),
    val id: Int? = null,
    val is_default: Boolean? = null,
    val location_area_encounters: String? = null,
    val moves: List<Move> = emptyList(),
    val name: String? = null,
    val order: Int? = null,
    val past_types: List<Any> = emptyList(),
    val species: Species? = null,
    val sprites: Sprites? = null,
    val stats: List<Stat> = emptyList(),
    val types: List<Type> = emptyList(),
    val weight: Int? = null
) {
    fun toPokemonDetailsEntity() = PokemonDetailsEntity(id, name, sprites)
}