package com.example.pokedexdata.model.objects.specifictypeinfo

data class HalfDamageFrom(
    val name: String,
    val url: String
)