package com.example.pokedexdata.model.local

import androidx.room.*
import com.example.pokedexdata.model.objects.specifictypeinfo.Pokemon
import com.example.pokedexdata.model.objects.alltypelist.AllTypeList
import com.example.pokedexdata.model.objects.entities.AllTypeListEntity
import com.example.pokedexdata.model.objects.entities.PokemonDetailsEntity
import com.example.pokedexdata.model.objects.entities.SpecificTypeInfoEntity

@Dao
interface PokedexDao {
    // Functions with no body that the rest of our code uses to interact with the db

    // Several functions commented out for now; I thought about using these to store pokemon types in the
    // db, but this might add unnecessary complexity. I'm holding off for now

/*
    // Insert Pokemon types into db so we can retrieve later
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPokemonTypes(vararg allTypeListEntity: AllTypeListEntity)
*/

    // Insert Pokemon from type into db so we can retrieve later
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertPokemonInType(vararg specificTypeInfoEntity: SpecificTypeInfoEntity)

/*
    // Retrieve Pokemon types from db
    @Query("SELECT * FROM AllTypeListEntity" )
    suspend fun getPokemonTypes() : List<AllTypeListEntity>
*/

    // Retrieve Pokemon in type from db
    @Query("SELECT * FROM SpecificTypeInfoEntity")
    suspend fun getPokemonInType() : List<SpecificTypeInfoEntity>

    // Retrieve a specific Pokemon's details from the db
    @Query("SELECT * FROM PokemonDetailsEntity")
    suspend fun getPokemonDetails() : PokemonDetailsEntity

   /* // Delete types from db
    @Query("DELETE FROM AllTypeListEntity")
    suspend fun deletePokemonTypes()*/

    // Delete Pokemon in type from db
    @Query("DELETE FROM SpecificTypeInfoEntity")
    suspend fun deletePokemonInType()


}