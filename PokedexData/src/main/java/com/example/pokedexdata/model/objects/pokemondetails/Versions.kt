package com.example.pokedexdata.model.objects.pokemondetails

data class Versions(
    val generationI: String = "",
    val generationII: String = "",
    val generationIII: String = "",
    val generationIV: String = "",
    val generationV: String = "",
    val generationVI: String = "",
    val generationVII: String = "",
    val generationVIII: String = ""
)