package com.example.pokedexdata.model.objects.pokemondetails

data class Ability(
    val ability: AbilityX,
    val is_hidden: Boolean,
    val slot: Int
)