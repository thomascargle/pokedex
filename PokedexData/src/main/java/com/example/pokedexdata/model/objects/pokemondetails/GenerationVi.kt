package com.example.pokedexdata.model.objects.pokemondetails

data class GenerationVi(
    val omegarubyAlphasapphire: String = "",
    val xY: String = ""
)