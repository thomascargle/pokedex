package com.example.pokedexdata.model.objects.pokemondetails

data class Type(
    val slot: Int,
    val type: TypeX
)