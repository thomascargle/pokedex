package com.example.pokedexdata.model.objects.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.pokedexdata.model.objects.pokemondetails.Sprites

@Entity
data class PokemonDetailsEntity(
    @PrimaryKey
    val id: Int? = null,
    val name: String? = null,
    val sprites: Sprites? = null,

    )
