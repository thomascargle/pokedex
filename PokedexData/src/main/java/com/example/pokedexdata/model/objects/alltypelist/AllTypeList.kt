package com.example.pokedexdata.model.objects.alltypelist

import com.example.pokedexdata.model.objects.entities.AllTypeListEntity

data class AllTypeList(
    val id: Int,
    val next: Any,
    val previous: Any,
    val types: List<Type>
) {
    fun toAllTypeListEntity() = AllTypeListEntity(id, types)
}