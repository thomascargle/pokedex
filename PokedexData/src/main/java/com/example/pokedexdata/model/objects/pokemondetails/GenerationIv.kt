package com.example.pokedexdata.model.objects.pokemondetails

data class GenerationIv(
    val diamondPearl: String = "",
    val heartgoldSoulsilver: String = "",
    val platinum: String = ""
)