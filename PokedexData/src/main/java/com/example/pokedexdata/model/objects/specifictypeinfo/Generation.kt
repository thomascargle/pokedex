package com.example.pokedexdata.model.objects.specifictypeinfo

data class Generation(
    val name: String,
    val url: String
)