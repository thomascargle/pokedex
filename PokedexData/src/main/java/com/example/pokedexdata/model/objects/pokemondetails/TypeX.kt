package com.example.pokedexdata.model.objects.pokemondetails

data class TypeX(
    val name: String,
    val url: String
)