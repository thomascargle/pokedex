package com.example.pokedexdata.model.objects.pokemondetails

data class GameIndice(
    val game_index: Int,
    val version: Version
)