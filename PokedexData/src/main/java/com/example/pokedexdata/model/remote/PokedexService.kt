package com.example.pokedexdata.model.remote

import com.example.pokedexdata.model.objects.pokemondetails.PokemonDetails
import com.example.pokedexdata.model.objects.specifictypeinfo.SpecificTypeInfo
import com.example.pokedexdata.model.objects.alltypelist.AllTypeList
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Path

interface PokedexService {

    companion object {
        // Our BaseURL can be everything except the final query for names/id
        private const val BASE_URL = "https://pokeapi.co/api/v2/"
        private const val TYPE_ENDPOINT = "type"

        // Create Retrofit
        fun getInstance(): PokedexService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()
    }

    // Functions to create API calls

    // Get types
    @GET(TYPE_ENDPOINT)
    suspend fun getTypes(): AllTypeList // Return the Types in objects.typelist.TypeList

    // Get Pokemon in that type. Will need type parameter
    @GET("type/{typeNumber}")
    suspend fun getPokemonInType(
        // Can use Path to get a URL without using a query
        @Path("typeNumber") typeNumber: Int
    ): SpecificTypeInfo // Return the TypeInfo in objects.typeinfo.TypeInfo

    // Get details of a specific pokemon. Will need name parameter
    @GET("pokemon/{name}")
    suspend fun getPokemonDetails(
        @Path("name") name: String
    ): PokemonDetails // Return PokemonDetails in object.pokemondetails.PokemonDetails
}