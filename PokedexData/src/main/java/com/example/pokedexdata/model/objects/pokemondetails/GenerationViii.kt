package com.example.pokedexdata.model.objects.pokemondetails

data class GenerationViii(
    val icons: String = ""
)