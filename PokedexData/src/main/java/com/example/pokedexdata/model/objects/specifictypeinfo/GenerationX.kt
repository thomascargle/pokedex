package com.example.pokedexdata.model.objects.specifictypeinfo

data class GenerationX(
    val name: String,
    val url: String
)