package com.example.pokedexdata.model.objects.alltypelist

data class Type(
    val name: String,
    val url: String
)