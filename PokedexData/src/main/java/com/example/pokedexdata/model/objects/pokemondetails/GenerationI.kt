package com.example.pokedexdata.model.objects.pokemondetails

data class GenerationI(
    val redBlue: String = "RedBlue",
    val yellow: String = "Yellow"
)