package com.example.pokedexdata.model.objects.specifictypeinfo

data class HalfDamageTo(
    val name: String,
    val url: String
)