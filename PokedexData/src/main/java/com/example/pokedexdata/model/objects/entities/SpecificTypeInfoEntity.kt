package com.example.pokedexdata.model.objects.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.pokedexdata.model.objects.specifictypeinfo.*

@Entity
data class SpecificTypeInfoEntity(
    @PrimaryKey
    val name: String? = null,
    val pokemon: List<Pokemon>? = emptyList()
)
