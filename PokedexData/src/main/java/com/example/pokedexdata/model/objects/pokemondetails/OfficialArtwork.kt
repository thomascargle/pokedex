package com.example.pokedexdata.model.objects.pokemondetails

data class OfficialArtwork(
    val front_default: String
)