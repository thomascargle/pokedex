package com.example.pokedexdata.model.repo

import android.content.Context
import com.example.pokedexdata.model.local.PokedexDB
import com.example.pokedexdata.model.objects.entities.PokemonDetailsEntity
import com.example.pokedexdata.model.objects.entities.SpecificTypeInfoEntity
import com.example.pokedexdata.model.objects.specifictypeinfo.Pokemon
import com.example.pokedexdata.model.remote.PokedexService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PokedexRepo(context: Context) {

    private val pokedexService = PokedexService.getInstance()
    private val pokedexDao = PokedexDB.getInstance(context).pokedexDao()

    // Get the pokemon types
    suspend fun getPokemonInType(typeNumber: Int) = withContext(Dispatchers.IO) {
        val result = pokedexService.getPokemonInType(typeNumber = typeNumber)
        val response = result.name.lowercase().toBooleanStrict()
        if (response) {
            pokedexDao.deletePokemonInType()
            val newlist = SpecificTypeInfoEntity(
                   name =  result.name,
                    pokemon = result.pokemon
                )

            pokedexDao.insertPokemonInType(newlist)
            return@withContext result.pokemon
        } else {
            return@withContext emptyList()
        }
    }

    suspend fun restorePokemonInType() = withContext(Dispatchers.IO) {
        return@withContext pokedexDao.getPokemonInType()
    }

    // Get the pokemon details
    suspend fun getPokemonDetails(name: String) : PokemonDetailsEntity = withContext(Dispatchers.IO) {
        return@withContext pokedexDao.getPokemonDetails()
    }
}

// In mainActivity onCreate
// Check if data has changed. If not, don't call API again
// If data hasn't changed, should already be cached

// onCreate: get API, compare API. If comparison fails, then they're different. That's when we go to a loading state
// Load while we're getting the data, then