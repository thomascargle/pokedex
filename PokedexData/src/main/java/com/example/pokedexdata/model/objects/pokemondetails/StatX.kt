package com.example.pokedexdata.model.objects.pokemondetails

data class StatX(
    val name: String,
    val url: String
)