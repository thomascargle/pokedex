package com.example.pokedexdata.model.objects.specifictypeinfo

data class Move(
    val name: String,
    val url: String
)