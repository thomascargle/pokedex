package com.example.pokedexdata.model.objects.pokemondetails

data class Form(
    val name: String,
    val url: String
)