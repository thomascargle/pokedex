package com.example.pokedexdata.model.objects.pokemondetails

data class Stat(
    val base_stat: Int,
    val effort: Int,
    val stat: StatX
)