package com.example.pokedexdata.model.objects.pokemondetails

data class MoveLearnMethod(
    val name: String,
    val url: String
)