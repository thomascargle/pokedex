package com.example.pokedexdata.model.objects.pokemondetails

data class XY(
    val front_default: String,
    val front_female: Any,
    val front_shiny: String,
    val front_shiny_female: Any
)