package com.example.pokedexdata.model.objects.pokemondetails

data class GenerationIi(
    val crystal: String = "",
    val gold: String = "",
    val silver: String = ""
)