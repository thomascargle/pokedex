package com.example.pokedexdata.model.objects.pokemondetails

data class FireredLeafgreen(
    val back_default: String,
    val back_shiny: String,
    val front_default: String,
    val front_shiny: String
)