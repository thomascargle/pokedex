package com.example.pokedexdata.model.objects.specifictypeinfo

data class NoDamageTo(
    val name: String,
    val url: String
)