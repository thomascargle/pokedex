package com.example.pokedexdata.model.objects.specifictypeinfo

data class DoubleDamageTo(
    val name: String,
    val url: String
)