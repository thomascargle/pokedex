package com.example.pokedexdata.model.objects.specifictypeinfo

data class Pokemon(
    val pokemon: PokemonX,
    val slot: Int
)