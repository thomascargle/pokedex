package com.example.pokedexdata.model.objects.pokemondetails

data class GenerationIii(
    val emerald: String = "",
    val fireredLeafgreen: String = "",
    val rubySapphire: String = ""
)