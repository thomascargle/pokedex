package com.example.pokedexdata.model.objects.specifictypeinfo

data class PokemonX(
    val name: String,
    val url: String
)